import os
import tool
import ZipObject
import email
from email.header import decode_header
from email.utils import parsedate_to_datetime
import re

def parse_eml(eml_fp, attr_dir):
    """
    eml文件解析
    :params eml_fp: eml文件路径
    :params attr_dir: 附件保存目录
    """
    eml_info = {}
    if not os.path.exists(attr_dir):
        os.makedirs(attr_dir)

    # 读取eml文件
    with open(eml_fp, "r") as file:
        eml_content = file.read()
    # 转为email对象
    msg = email.message_from_string(eml_content)

    # 邮件主题
    subject_bytes, subject_encode = decode_header(msg["Subject"])[0]
    if subject_encode:
        subject = subject_bytes.decode(subject_encode)
    else:
        subject = subject_bytes
    # print("主题：", subject)
    eml_info['subject'] = subject

    # 邮件发件人
    from_ip = re.search("<(.*)>", msg["from"]).group(1)
    # print("发件人邮箱：", from_ip)
    eml_info['sender_mail'] = from_ip


    from_name = decode_header(msg["from"].split("<")[0].strip())
    if from_name:
        if from_name[0] and from_name[0][1]:
            from_n = from_name[0][0].decode(from_name[0][1])
        else:
            from_n = from_name[0][0]
    # print("发件人名称：", from_n)
    eml_info['sender_name'] = from_n

    # 邮件时间
    received_date = parsedate_to_datetime(msg["date"])
    # print("接收时间：", received_date)
    eml_info['recv_date'] = str(received_date.date()).replace('-', '/')

    body = ''
    # 邮件正文及附件
    eml_info['files'] = []
    for par in msg.walk():
        if not par.is_multipart():  # 判断是否为multipart，里面的数据不需要
            name = par.get_param("name")  # 获取附件的文件名
            if name:  
                # 附件
                fname = decode_header(name)[0]
                if fname[1]:
                    attr_name = fname[0].decode(fname[1])
                else:
                    attr_name = fname[0]
                # print("附件名:", attr_name)
                eml_info['files'].append(attr_name)

                # 解码附件内容
                attr_data = par.get_payload(decode=True)
                attr_fp = os.path.join(attr_dir, attr_name)
                with open(attr_fp, 'wb') as f_write:
                    f_write.write(attr_data)
            # else:  
            #     # 正文
            #     text_char = par.get_content_charset()
            #     if "text/plain" in par["content-type"]:  # 文本正文
            #         body = par.get_payload(decode=True).decode(text_char).replace('&nbsp;', '')
                    # eml_info['body'] = body
                    # print("邮件正文：", body)
                    # print("-" * 60)
                # else:  # html格式正文
                #     html_body = par.get_payload(decode=True).decode(text_char)
                #     print("HTML正文：", html_body)
    return eml_info





class EmailObject:
    def __init__(self, email_name) -> None:
        self.email_name = email_name
        self.email_path = tool.get_email_path(email_name)
        self.parse_enable = True
        self.eml_info = {}
        self.zip_list = []
        self.scan_flag = False

    def clear(self):
        self.parse_enable = True
        self.scan_flag = False
        self.zip_list.clear()
        self.eml_info = {}

    def scan(self):
        self.clear()

        if not os.path.exists(self.email_path):
            tool.log_show('ERROR', self.email_path, '文件路径存在')
            return False
        else:
            self.eml_info = parse_eml(self.email_path, tool.ZIP_PATH)
            for zip_name in self.eml_info['files']:
                zip_obj = ZipObject.ZipObject(self.eml_info, self.email_name.replace(".eml", ""), zip_name)
                zip_obj.scan()
                self.zip_list.append(zip_obj)

            return True
        

    def parse(self, parse_tree: dict):
        zip_obj: ZipObject.ZipObject = None
        parse_result = []

        for parse_zips in parse_tree:
            for parse_zip_name, parse_files in parse_zips.items():
                zip_obj = self.find_zip_obj(parse_zip_name)
                parse_result += zip_obj.parse(parse_files['childs'])

        return parse_result
    
    def find_zip_obj(self, zip_name):
        for zip_obj in self.zip_list:
            if zip_obj.zip_name == zip_name:
                return zip_obj
        return None

    def get_list(self):
        list_info= {}
        zip_obj: ZipObject.ZipObject = None

        for zip_obj in self.zip_list:
            list_info[zip_obj.zip_name] = zip_obj.get_list()
        
        return list_info
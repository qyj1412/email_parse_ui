# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWidget.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QDockWidget, QGridLayout, QHBoxLayout,
    QHeaderView, QMainWindow, QMenu, QMenuBar,
    QPlainTextEdit, QPushButton, QSizePolicy, QStatusBar,
    QTreeWidgetItem, QVBoxLayout, QWidget)

from EmailTreeWidget import EmailTreeWidget

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1191, 726)
        self.actionStartParse = QAction(MainWindow)
        self.actionStartParse.setObjectName(u"actionStartParse")
        self.actionParse = QAction(MainWindow)
        self.actionParse.setObjectName(u"actionParse")
        self.actionEmails = QAction(MainWindow)
        self.actionEmails.setObjectName(u"actionEmails")
        self.actionParsedZips = QAction(MainWindow)
        self.actionParsedZips.setObjectName(u"actionParsedZips")
        self.actionParsedDirs = QAction(MainWindow)
        self.actionParsedDirs.setObjectName(u"actionParsedDirs")
        self.action1 = QAction(MainWindow)
        self.action1.setObjectName(u"action1")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout_2 = QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.email_select_button = QPushButton(self.centralwidget)
        self.email_select_button.setObjectName(u"email_select_button")
        self.email_select_button.setMinimumSize(QSize(111, 41))

        self.horizontalLayout.addWidget(self.email_select_button)

        self.zip_select_button = QPushButton(self.centralwidget)
        self.zip_select_button.setObjectName(u"zip_select_button")
        self.zip_select_button.setMinimumSize(QSize(111, 41))

        self.horizontalLayout.addWidget(self.zip_select_button)

        self.file_select_button = QPushButton(self.centralwidget)
        self.file_select_button.setObjectName(u"file_select_button")
        self.file_select_button.setMinimumSize(QSize(111, 41))

        self.horizontalLayout.addWidget(self.file_select_button)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.expand_button = QPushButton(self.centralwidget)
        self.expand_button.setObjectName(u"expand_button")
        self.expand_button.setMinimumSize(QSize(111, 41))

        self.horizontalLayout_2.addWidget(self.expand_button)

        self.expand_button_2 = QPushButton(self.centralwidget)
        self.expand_button_2.setObjectName(u"expand_button_2")
        self.expand_button_2.setMinimumSize(QSize(111, 41))

        self.horizontalLayout_2.addWidget(self.expand_button_2)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.email_tree = EmailTreeWidget(self.centralwidget)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.email_tree.setHeaderItem(__qtreewidgetitem)
        self.email_tree.setObjectName(u"email_tree")
        self.email_tree.setSortingEnabled(False)
        self.email_tree.setAnimated(False)
        self.email_tree.setWordWrap(False)
        self.email_tree.setHeaderHidden(False)
        self.email_tree.header().setHighlightSections(False)
        self.email_tree.header().setProperty("showSortIndicator", False)

        self.verticalLayout.addWidget(self.email_tree)


        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1191, 22))
        self.menuOptions = QMenu(self.menubar)
        self.menuOptions.setObjectName(u"menuOptions")
        self.menuSource = QMenu(self.menubar)
        self.menuSource.setObjectName(u"menuSource")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.dockWidget_3 = QDockWidget(MainWindow)
        self.dockWidget_3.setObjectName(u"dockWidget_3")
        self.dockWidget_3.setMinimumSize(QSize(112, 112))
        self.dockWidget_3.setFloating(False)
        self.dockWidget_3.setFeatures(QDockWidget.DockWidgetFloatable|QDockWidget.DockWidgetMovable|QDockWidget.DockWidgetVerticalTitleBar)
        self.dockWidget_3.setAllowedAreas(Qt.NoDockWidgetArea)
        self.dockWidgetContents_3 = QWidget()
        self.dockWidgetContents_3.setObjectName(u"dockWidgetContents_3")
        self.gridLayout = QGridLayout(self.dockWidgetContents_3)
        self.gridLayout.setObjectName(u"gridLayout")
        self.log_shower = QPlainTextEdit(self.dockWidgetContents_3)
        self.log_shower.setObjectName(u"log_shower")
        font = QFont()
        font.setFamilies([u"JetBrains Mono"])
        self.log_shower.setFont(font)
        self.log_shower.setReadOnly(True)
        self.log_shower.setOverwriteMode(False)

        self.gridLayout.addWidget(self.log_shower, 0, 0, 1, 1)

        self.dockWidget_3.setWidget(self.dockWidgetContents_3)
        MainWindow.addDockWidget(Qt.LeftDockWidgetArea, self.dockWidget_3)

        self.menubar.addAction(self.menuOptions.menuAction())
        self.menubar.addAction(self.menuSource.menuAction())
        self.menuOptions.addAction(self.actionStartParse)
        self.menuOptions.addAction(self.actionParse)
        self.menuSource.addAction(self.actionEmails)
        self.menuSource.addAction(self.actionParsedZips)
        self.menuSource.addAction(self.actionParsedDirs)
        self.menuSource.addAction(self.action1)

        self.retranslateUi(MainWindow)
        self.menubar.triggered.connect(MainWindow.menu_triggle)
        self.email_tree.doubleClicked.connect(MainWindow.email_tree_doubleclick)
        self.email_select_button.clicked.connect(self.email_tree.email_select_slot)
        self.zip_select_button.clicked.connect(self.email_tree.zip_select_slot)
        self.file_select_button.clicked.connect(self.email_tree.file_select_slot)
        self.expand_button.clicked.connect(self.email_tree.expandAll)
        self.expand_button_2.clicked.connect(self.email_tree.collapseAll)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"EmailParse", None))
        self.actionStartParse.setText(QCoreApplication.translate("MainWindow", u"Scan", None))
        self.actionParse.setText(QCoreApplication.translate("MainWindow", u"Parse", None))
        self.actionEmails.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u90ae\u4ef6\u5217\u8868", None))
        self.actionParsedZips.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u538b\u7f29\u5305\u754c\u9762", None))
        self.actionParsedDirs.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u89e3\u538b\u6587\u4ef6\u754c\u9762", None))
        self.action1.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u8f93\u51fa\u6587\u4ef6\u754c\u9762", None))
        self.email_select_button.setText(QCoreApplication.translate("MainWindow", u"\u90ae\u4ef6\u53d6\u6d88\u5168\u9009", None))
        self.zip_select_button.setText(QCoreApplication.translate("MainWindow", u"\u538b\u7f29\u5305\u53d6\u6d88\u5168\u9009", None))
        self.file_select_button.setText(QCoreApplication.translate("MainWindow", u"\u6587\u4ef6\u53d6\u6d88\u5168\u9009", None))
        self.expand_button.setText(QCoreApplication.translate("MainWindow", u"\u5168\u90e8\u5c55\u5f00", None))
        self.expand_button_2.setText(QCoreApplication.translate("MainWindow", u"\u5168\u90e8\u6298\u53e0", None))
        self.menuOptions.setTitle(QCoreApplication.translate("MainWindow", u"Options", None))
        self.menuSource.setTitle(QCoreApplication.translate("MainWindow", u"SourceFile", None))
        self.dockWidget_3.setWindowTitle(QCoreApplication.translate("MainWindow", u"logInfo", None))
    # retranslateUi


type_servo = (
    "DS", "MS","DF", "MF","伺服"
)

type_step = (
    "DP", "MP", "步进"
)

type_freq = (
    "VH", "变频"
)

need_rework = (
    "返工", 
)

need_rework_exclude = (
    "不返工", "无需返工", 
)


filename_replace = {
    "MP3-86H100-2000.docx": "投产MP3-86H100-2000.docx",
    "RD-DRRD-SW-001-202312(01).docx": "ECN-RD-DRRD-SW-001-202312(01).docx",
    "20231101全新外观5L1的CPU板1-3KW上拉改下拉（192）-王彬.docx": "20231101ECN全新外观5L1的CPU板1-3KW上拉改下拉（192）-王彬.docx",
    
}

def preview_data(data: str, preview_buffer: tuple[str]):
    for i in preview_buffer:
        if data.find(i) >= 0:
            return True
    return False


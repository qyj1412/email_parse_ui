import re, sys
import os, subprocess
import email
import shutil
from email.header import decode_header
from email.utils import parsedate_to_datetime
from docx import Document
import pdfplumber
import pandas as pd
import preview
#from LAC import LAC
#from ltp import LTP
#lac = LAC(mode="lac")
#ltp = LTP("./base")  # 默认加载 Small 模型，下载的路径是：~/.cache/torch/ltp

'''

def lac_username(sentences: str):
    # 装载LAC模型
    global lac
    user_name_list = []
    lac_result = lac.run(sentences)
    for index, lac_label in enumerate(lac_result[1]):
        if lac_label == "PER":
            user_name_list.append(lac_result[0][index])
            
    print('lac find in ', sentences)
    print('lac find   >', user_name_list)
    if user_name_list == []:
        return ''
    return user_name_list[0]

def ltp_username(sentences: str) -> list:
    global ltp
    seg, hidden = ltp.seg([sentences])  # 分词
    nh_user_list = []
    pos_index_values = ltp.pos(hidden)
    # seg 是 list to list 的格式
    for index, seg_i in enumerate(seg):
        pos_values = pos_index_values[index]
        for _index, _pos in enumerate(pos_values):
            if _pos == "nh":
                nh_user_list.append(seg_i[_index])
    print('ltp find in ', sentences)
    print('ltp find   >', user_name_list)
    if nh_user_list == []:
        return ''
    return nh_user_list[0]
'''

def parse_eml(eml_fp, attr_dir):
    """
    eml文件解析
    :params eml_fp: eml文件路径
    :params attr_dir: 附件保存目录
    """
    eml_info = {}
    if not os.path.exists(attr_dir):
        os.makedirs(attr_dir)

    # 读取eml文件
    with open(eml_fp, "r") as file:
        eml_content = file.read()
    # 转为email对象
    msg = email.message_from_string(eml_content)

    # 邮件主题
    subject_bytes, subject_encode = decode_header(msg["Subject"])[0]
    if subject_encode:
        subject = subject_bytes.decode(subject_encode)
    else:
        subject = subject_bytes
    # print("主题：", subject)
    eml_info['subject'] = subject

    # 邮件发件人
    from_ip = re.search("<(.*)>", msg["from"]).group(1)
    # print("发件人邮箱：", from_ip)
    eml_info['sender_mail'] = from_ip


    from_name = decode_header(msg["from"].split("<")[0].strip())
    if from_name:
        if from_name[0] and from_name[0][1]:
            from_n = from_name[0][0].decode(from_name[0][1])
        else:
            from_n = from_name[0][0]
    # print("发件人名称：", from_n)
    eml_info['sender_name'] = from_n

    # 邮件时间
    received_date = parsedate_to_datetime(msg["date"])
    # print("接收时间：", received_date)
    eml_info['recv_date'] = str(received_date.date()).replace('-', '/')

    body = ''
    # 邮件正文及附件
    for par in msg.walk():
        if not par.is_multipart():  # 判断是否为multipart，里面的数据不需要
            name = par.get_param("name")  # 获取附件的文件名
            if name:  
                # 附件
                fname = decode_header(name)[0]
                if fname[1]:
                    attr_name = fname[0].decode(fname[1])
                else:
                    attr_name = fname[0]
                # print("附件名:", attr_name)
                eml_info['file'] = attr_name

                # 解码附件内容
                attr_data = par.get_payload(decode=True)
                attr_fp = os.path.join(attr_dir, attr_name)
                with open(attr_fp, 'wb') as f_write:
                    f_write.write(attr_data)
            else:  
                # 正文
                text_char = par.get_content_charset()
                if "text/plain" in par["content-type"]:  # 文本正文
                    body = par.get_payload(decode=True).decode(text_char).replace('&nbsp;', '')
                    eml_info['body'] = body
                    # print("邮件正文：", body)
                    # print("-" * 60)
                # else:  # html格式正文
                #     html_body = par.get_payload(decode=True).decode(text_char)
                #     print("HTML正文：", html_body)
    return eml_info



def parse_word(file_name):
    lo = {} # 存储每一行去重后的数据
    doc = Document(file_name)
    table = doc.tables[0]
    
    for row in range(0, len(table.rows)):
        row_list = []
        for col in range(0,len(table.row_cells(row))):  # 提取row行的全部列数据
            row_list.append(table.cell(row,col).text.replace('\n','').replace(' ', '')) # 去除字符串中的特殊字符，并添加到临时列表中
        lo[row] = (sorted(set(row_list), key = row_list.index)) # 在不变顺序的前提下，去除List中的重复项
        # print(row, ":len(", len(lo[row]),'):',lo[row])
        
    return tuple(lo.values())

def parse_pdf(file_name):
    data = []
    result_df = pd.DataFrame()
    pdf = pdfplumber.open(file_name)
    for page in pdf.pages:
        table = page.extract_table()
        df_detail = pd.DataFrame(table)
        result_df = pd.concat([df_detail, result_df], ignore_index=True)

    return result_df.values.tolist()
        

def parse_ecn(data: list):
    # 打印出每行的数据观察相关格式特征
    
    ecn_data = {'product_code': '', 'reason': '', 'code': '', 'starter': '', 'type': 'ecn'}
    print(ecn_data)
    for row, line in enumerate(data):
        line = tuple(filter(lambda x: True if x != None and x != '' else False, line))
        print(f"line {row}, {line}")
        
        for index, lineitem in enumerate(line): # 遍历每行的每一个元素
            if lineitem == 'ECN编号':
                ecn_data['code'] = line[index+1]
            elif lineitem == '发起人':
                ecn_data['starter'] = '' if line[-1] == lineitem else re.sub(r'\d+', '', line[-1]).replace(".", "").replace("-", "").replace("/", "")
            elif lineitem == '产品型号':
                ecn_data['product_code'] = line[index+1]
        
        if row == 6:
            ecn_data['reason'] += ''.join(line)
            ecn_data['reason'] += '\n'
        if row == 7:
            if line[0].find("涉及变更的物料、产品处理意见") > 0:
                ecn_data['reason'] += line[0][:line[0].find("涉及变更的物料、产品处理意见")-3]
            else:
                ecn_data['reason'] += line[0]
            
            


    return ecn_data

def parse_production(data: list):
    reason_startrow = 0
    reason_endrow = 0
    start_reason = 0
    reason = ''
    
    product_data = {'product_code': '', 'reason': '', 'code': '', 'starter': '', 'type': 'product'}

    for row, line in enumerate(data):# 遍历每一行
        line = tuple(filter(lambda x: True if x != None and x != '' else False, line))
        print(f"line {row}, {line}")
        
        for index, lineitem in enumerate(line): # 遍历每行的每一个元素
            if lineitem == '产品型号': 
                try:
                    product_data['product_code'] = line[index+1]
                except Exception as E:
                    print("没找到产品型号, 默认为空")
                    product_data['product_code'] = ''
                reason_startrow = row
                start_reason = 10000
                break
            elif lineitem == '生产建议':
                reason_endrow = row
                start_reason = 1
                break
            elif lineitem == '编号': # 找投产编号
                try:
                    product_data['code'] = line[index+1]
                except Exception as E:
                    print("没找到编号, 默认为空")
                    product_data['code'] = ''
            elif lineitem == '发起人':
                product_data['starter'] = '' if line[-1] == lineitem else re.sub(r'\d+', '', line[-1]).replace(".", "").replace("-", "").replace("/", "")
            
        if start_reason > 0:
            start_reason = start_reason - 1
            reason += ''.join(line)
            reason += '\n'
            product_data['reason'] = reason
    
    return product_data


def parse_data(root_path: str, file_name: str):
    # 判断规则: 名称包含ECN或者投产字样 并且 文件类型为PDF或WORD字样
    target_file = os.path.join(root_path, file_name)
    if file_name.find("签字") >= 0: # 签字版扫描会有问题 不进行处理
        return None
    
    if file_name in preview.filename_replace.keys():
        file_name = preview.filename_replace[file_name]
    

    if (file_name.upper().find('ECN') >= 0 or file_name.upper().find('工程变更通知单') >= 0 or file_name.upper().find('RD-DRRD-SW-') >= 0 or file_name.upper().find('投产') >= 0) and (file_name.endswith('.pdf') or file_name.endswith('.doc') or file_name.endswith('.docx')):
        if file_name.endswith('.doc') or file_name.endswith('.docx'):
            print("开始解析word", target_file)
            data = parse_word(target_file)
        elif file_name.endswith('.pdf'):
            print("开始解析pdf", target_file)
            data = parse_pdf(target_file)
        else:
            return None
    
        if file_name.upper().find('ECN') >= 0 or file_name.upper().find('工程变更通知单') >= 0 or file_name.upper().find('RD-DRRD-SW-') >= 0:
            print("开始解析ECN", target_file)
            return parse_ecn(data)
        elif file_name.upper().find('投产') >= 0:
            print("开始解析投产文件", target_file)
            return parse_production(data)
        else:
            return None
    return None
    

if __name__ == '__main__':
    eml_root_path = "dir"
    eml_output_path = "output"
    fast_copy = 'tool/FastCopy/FastCopy.exe'
    root_path = os.getcwd()
    tool_7z_path = os.path.join(root_path, r"tool/7z/7z.exe")
    fast_copy_path = os.path.join(root_path, fast_copy)

    # 程序运行之前会先把输出目录清空, 防止旧的数据残留
    try:
        if os.path.exists(eml_output_path):
            os.system(f"rmdir /Q /S {eml_output_path}")
        os.mkdir(eml_output_path)
            
        print('清空输出目录成功')
    except Exception as E:
        print(eml_output_path, "不存在", str(E))
        sys.exit(-1)

    # 每个ecn email邮件的信息
    emls_info = {}
    # 解析所有的压缩文件到一个目录, 并用eml的名称来区分
    print('开始解析eml')
    for eml in os.listdir(eml_root_path):
        if not eml.endswith('.eml'):
            continue
        
        print('开始解析> ', os.path.join(eml_root_path, eml))
        info = parse_eml(os.path.join(eml_root_path, eml), os.path.join(eml_output_path, eml.replace(".eml", "")))
        emls_info[eml.replace(".eml", "")] = info
        print('完成解析> ', os.path.join(eml_root_path, eml))

    for ecn_dir in os.listdir(eml_output_path):
        print('\n\n\n=============================================\n开始解析文件', ecn_dir)

        os.chdir(root_path)
        os.chdir(os.path.join(eml_output_path, ecn_dir))

        # 使用7z来解压
        print('开始解压文件', ecn_dir)
        print(tool_7z_path)
        files = os.listdir()
        print(os.getcwd())
        for index, file in enumerate(files):
            if os.path.splitext(file)[-1] in ['.zip', '.rar', '.7z']:
                subprocess.run([tool_7z_path, 'x', file, f'-ozipfile_{index}'])

        print('解压文件完成', ecn_dir)

        # 遍历有关ecn的文件获取ecn信息
        parse_ok = 0
        for root, _, ecn_files in os.walk("./"):
            for ecn_file in ecn_files:
                if ecn_file.find("~$") >=0 :
                    continue
                emls_info[ecn_dir]['ecn_file_path'] = os.path.join(root, ecn_file)
                data = parse_data(root, ecn_file)
                if isinstance(data, dict):
                    emls_info[ecn_dir] = dict(**emls_info[ecn_dir], **data)
                    parse_ok = 1
                    break
            if parse_ok:
                break
        if parse_ok:
            print('解析文件完毕', ecn_dir, '\n=============================================\n\n\n')
        else:
            print('没有找到对应的可解析文件, 请手动操作')


    print('\n\n\n开始构建Excel', '\n=============================================\n\n\n')


    excel_data = {'序号': [], 'ECN编号（研发）': [], '接收时间': [], '产品类型': [],
                '变更内容（发布）': [], '变更内容': [], '半成品、成品是否返工': [] , 
                '切换类型（自然切换、立即切换）': [], 
                'ECN原因（缺料替代、换料投产、降本、质量问题、产品性能、产品外观提升、提升效率、设计问题、BOM修改、版本升级、新品投产等）': [], 
                '方案设计': [], '具体实施日期（自然切换：不跟进立即切换：写日期）': [], '闭环情况(CLOSE/OPEN/NA)': [], 
                '相关内容是否已放共享(是/否)': [], 
                '备注': [], '邮件内容': [], '邮件类型': []
    }
    
    for name, val in emls_info.items():
        try:     
            index = ''
            ecn_code = ''
            recv_date = ''
            product_type = ''
            change_info_publish = ''
            change_info = ''
            rework = ''
            change_type = ''
            ecn_reason = ''
            design = ''
            work_date = ''
            close_open = ''
            is_shared = ''
            other = ''
            mail_info = ''
            email_type = ''
            
            
            index = ''
            ecn_code = val['code']
            recv_date = val['recv_date']
            
            product_string = '' # 产品类型 对应了D列 来源: 邮件内容O列
            # 匹配产品类型
            if preview.preview_data(name.upper(), preview.type_servo):
                product_type = '伺服'
                product_string = "伺服"      
            elif preview.preview_data(name.upper(), preview.type_step):
                product_type = '步进'
                product_string = "步进"  
            elif preview.preview_data(name.upper(), preview.type_freq):
                product_type = '变频'
                product_string = "变频"  
            else:
                product_type = ''# 默认是空
            change_info_publish = val['reason']
            
            email_type = val['type']
            if val['type'] == 'product': # 区分邮件是ECN还是投产
                type_string = '' # 驱动器, 电机, 变频器, 步进驱动, 步进电机, 定制机 对应了F列的内容, 来源: 邮件内容
                ECN_string = "通用" # 对应了I列的ECN原因, 来源: 邮件内容或者变更内容发布(附件word或者PDF)
                if preview.preview_data(name, ['定制']) or preview.preview_data(val['reason'], ['定制']):
                    ECN_string = "定制"
                    type_string = "定制机"
                else:                
                    if preview.preview_data(name.upper(), ['DS']):
                        type_string = '驱动器'
                    elif preview.preview_data(name.upper(), ['MS']):
                        type_string = '电机'
                    elif preview.preview_data(name.upper(), ['VH']):
                        type_string = '变频器'
                    elif preview.preview_data(name.upper(), ['DP']):
                        type_string = '步进驱动'
                    elif preview.preview_data(name.upper(), ['MP']):
                        type_string = '步进电机'

                
                change_info = (f'新投产{type_string}： {val["product_code"]}')
                rework = '否'
                change_type = '立即切换'
                ecn_reason = f"新品投产({ECN_string})"
                is_shared = '是'
                other = '配置表，图纸更新'
            else:
                if name.find(":") >= 0:
                    change_info = name[name.index(":")+1:]
                elif name.find("：") >= 0:
                    change_info = name[name.index("：")+1:]
                else:
                    change_info = ''
                ecn_reason = ''
                is_shared = '否'
                other = '/'
            
                """
                if preview.preview_data(val['reason'], preview.need_rework) and not preview.preview_data(val['reason'], preview.need_rework_exclude):
                    rework = append('返工')
                    change_type = ('立即切换')
                else:
                """
                rework = ''
                change_type = ''
            
            
                        
            design = val['starter']
            work_date = ''
            close_open = ''
            mail_info = name
            
            excel_data['邮件类型'].append(email_type)
            excel_data['序号'].append(index)
            excel_data['ECN编号（研发）'].append(ecn_code)
            excel_data['接收时间'].append(recv_date)
            excel_data['产品类型'].append(product_type)
            excel_data['变更内容（发布）'].append(change_info_publish)
            excel_data['变更内容'].append(change_info)
            excel_data['半成品、成品是否返工'].append(rework)
            excel_data['切换类型（自然切换、立即切换）'].append(change_type)
            excel_data['ECN原因（缺料替代、换料投产、降本、质量问题、产品性能、产品外观提升、提升效率、设计问题、BOM修改、版本升级、新品投产等）'].append(ecn_reason)
            excel_data['方案设计'].append(design)
            excel_data['具体实施日期（自然切换：不跟进立即切换：写日期）'].append(work_date)
            excel_data['闭环情况(CLOSE/OPEN/NA)'].append(close_open)
            excel_data['相关内容是否已放共享(是/否)'].append(is_shared)
            excel_data['备注'].append(other)
            excel_data['邮件内容'].append(mail_info)
        except Exception as E:
            excel_data['邮件类型'].append('')
            excel_data['序号'].append('')
            excel_data['ECN编号（研发）'].append('')
            excel_data['接收时间'].append('')
            excel_data['产品类型'].append('')
            excel_data['变更内容（发布）'].append('')
            excel_data['变更内容'].append('')
            excel_data['半成品、成品是否返工'].append('')
            excel_data['切换类型（自然切换、立即切换）'].append('')
            excel_data['ECN原因（缺料替代、换料投产、降本、质量问题、产品性能、产品外观提升、提升效率、设计问题、BOM修改、版本升级、新品投产等）'].append('')
            excel_data['方案设计'].append('')
            excel_data['具体实施日期（自然切换：不跟进立即切换：写日期）'].append('')
            excel_data['闭环情况(CLOSE/OPEN/NA)'].append('')
            excel_data['相关内容是否已放共享(是/否)'].append('')
            excel_data['备注'].append('')
            excel_data['邮件内容'].append(name)
            print(f"{name} 解析出现了问题： \n{str(E)}")
            continue


    
    print("\n\n\n开始整合投产文件 CAD&3D")
    os.chdir(root_path) # 切换为根目录
    CAD_PATH = os.path.join(root_path, 'output\!CAD') # CAD文件的输出目录
    THREED_PATH = os.path.join(root_path, 'output\!3D') # 3D文件的输出目录
    if not os.path.exists(CAD_PATH): # 文件夹不存在就创建
        os.makedirs(CAD_PATH)
    if not os.path.exists(THREED_PATH):
        os.makedirs(THREED_PATH)
       
    for index, ecn_dir in enumerate(filter(lambda x: False if x == '!CAD' or x == '!3D' else True, os.listdir(eml_output_path))): # 遍历所有附件目录
        os.chdir(os.path.join(root_path, eml_output_path)) # 切换到输出目录
    
        if ecn_dir.find("投产") < 0: # 找到是投产文件
            continue
        
        os.chdir(ecn_dir) # 切换到附件目录
        ecn_root_path = os.getcwd()
        find_dir_ok = False
        find_dir_file_ok = False
        
        os_path = os.walk("./")
        for root, _, eml_files in os_path:
            os.chdir(ecn_root_path)
            if root.find("图") < 0:
                continue
            
            
            os.chdir(root)
            
            find_dir_ok = True
            print(root)
            for file in eml_files:
                if file.upper().endswith('DWG') or file.upper().endswith('PDF'): # 找到所有的CAD文件
                    print(' '.join([fast_copy_path, '/cmd=sync', '/auto_close', file, f'/to={CAD_PATH}']))
                    subprocess.run([fast_copy_path, '/cmd=sync', '/auto_close', file, f'/to={CAD_PATH}'])
                    #os.system(f"{fast_copy_path} {file} /cmd=sync  /to={CAD_PATH}")
                    find_dir_file_ok = True
                elif file.upper().endswith('IGS') or file.upper().endswith('STEP'): # 找到所有的3D文件
                    print(' '.join([fast_copy_path, '/cmd=sync', '/auto_close', file, f'/to={THREED_PATH}']))
                    subprocess.run([fast_copy_path, '/cmd=sync', '/auto_close', file, f'/to={THREED_PATH}'])
                    #os.system(f"{fast_copy_path} {file} /cmd=sync /auto_close /to={THREED_PATH}")
                    find_dir_file_ok = True

            
            os.chdir(ecn_root_path)
            
        
        if not find_dir_ok:
            print(f"{ecn_dir} 没有找到对应的 外形图及二维图 文件夹")
            if excel_data['邮件类型'][index] == 'product':
                excel_data['备注'][index] = '配置表更新，无图纸'

        elif not find_dir_file_ok:
            print(f'{ecn_dir} 没有找到对应的 CAD文件或者3D文件')
            if excel_data['邮件类型'][index] == 'product':
                excel_data['备注'][index] = '配置表更新，无图纸'


    os.chdir(root_path)
    
    # 删除不需要的表格内容
    del excel_data['邮件类型']
    
    df_excel = pd.DataFrame(excel_data)
    
    df_excel.to_csv(os.path.join(eml_output_path, 'output.csv'), index=False)
    print("生成完毕, 生成位置", os.path.join(eml_output_path, 'output.csv'))

    print('整合完成')


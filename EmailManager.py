from EmailObject import EmailObject
from PySide6.QtWidgets import *
import tool
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
import time



class EmailManager:
    def __init__(self) -> None:
        self.parse_result = []
        self.obj_list = []
        self.clear()


    def clear(self):
        self.obj_list.clear()
        self.parse_result = []

    def scan(self):
        self.clear()

        for eml in tool.get_emails():
            obj = EmailObject(eml)

            if obj.scan():
                self.obj_list.append(obj)

        return True


    def get_list(self):
        list_info = {}
        obj: EmailObject= None
        for obj in self.obj_list:
            list_info[obj.email_name] = obj.get_list()

        return list_info


    def parse(self, parse_tree: dict):
        parse_result = []
        obj: EmailObject = None
        for obj in self.obj_list:
            if obj.email_name in parse_tree.keys():
                parse_result += obj.parse(parse_tree[obj.email_name]['childs'])
        return parse_result
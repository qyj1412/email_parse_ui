import tool
import preview
import pdfplumber
import pandas as pd
from docx import Document
import os
import re
import copy
import subprocess

ECN_KEY_WORD = (
    'ECN', '工程变更通知单', 'RD-DRRD-SW-'
)


PRODUCT_KEY_WORD = (
    '投产', 
)


def parse_ecn(data: list):
    # 打印出每行的数据观察相关格式特征
    
    ecn_data = {'product_code': '', 'reason': '', 'code': '', 'starter': '', 'type': 'ecn'}
    for row, line in enumerate(data):
        line = tuple(filter(lambda x: True if x != None and x != '' else False, line))
        # print(f"line {row}, {line}")
        
        for index, lineitem in enumerate(line): # 遍历每行的每一个元素
            if lineitem == 'ECN编号':
                ecn_data['code'] = line[index+1]
            elif lineitem == '发起人':
                ecn_data['starter'] = '' if line[-1] == lineitem else re.sub(r'\d+', '', line[-1]).replace(".", "").replace("-", "").replace("/", "")
            elif lineitem == '产品型号':
                ecn_data['product_code'] = line[index+1]
        
        if row == 6:
            ecn_data['reason'] += ''.join(line)
            ecn_data['reason'] += '\n'
        if row == 7:
            if line[0].find("涉及变更的物料、产品处理意见") > 0:
                ecn_data['reason'] += line[0][:line[0].find("涉及变更的物料、产品处理意见")-3]
            else:
                ecn_data['reason'] += line[0]
            
            


    return ecn_data



def parse_production(data: list):
    reason_startrow = 0
    reason_endrow = 0
    start_reason = 0
    reason = ''
    
    product_data = {'product_code': '', 'reason': '', 'code': '', 'starter': '', 'type': 'product'}

    for row, line in enumerate(data):# 遍历每一行
        line = tuple(filter(lambda x: True if x != None and x != '' else False, line))
        # print(f"line {row}, {line}")
        
        for index, lineitem in enumerate(line): # 遍历每行的每一个元素
            if lineitem == '产品型号': 
                try:
                    product_data['product_code'] = line[index+1]
                except Exception as E:
                    print("没找到产品型号, 默认为空")
                    product_data['product_code'] = ''
                reason_startrow = row
                start_reason = 10000
                break
            elif lineitem == '生产建议':
                reason_endrow = row
                start_reason = 1
                break
            elif lineitem == '编号': # 找投产编号
                try:
                    product_data['code'] = line[index+1]
                except Exception as E:
                    print("没找到编号, 默认为空")
                    product_data['code'] = ''
            elif lineitem == '发起人':
                product_data['starter'] = '' if line[-1] == lineitem else re.sub(r'\d+', '', line[-1]).replace(".", "").replace("-", "").replace("/", "")
            
        if start_reason > 0:
            start_reason = start_reason - 1
            reason += ''.join(line)
            reason += '\n'
            product_data['reason'] = reason
    
    return product_data




def is_ecn(file_email_name: str):
    for key in ECN_KEY_WORD:
        if file_email_name.find(key) >= 0:
            return True
    return False


def is_product(file_email_name: str):
    for key in PRODUCT_KEY_WORD:
        if file_email_name.find(key) >= 0:
            return True
    return False


def parse_word(word_path):
    lo = {} # 存储每一行去重后的数据
    doc = Document(word_path)
    table = doc.tables[0]
    
    for row in range(0, len(table.rows)):
        row_list = []
        for col in range(0,len(table.row_cells(row))):  # 提取row行的全部列数据
            row_list.append(table.cell(row,col).text.replace('\n','').replace(' ', '')) # 去除字符串中的特殊字符，并添加到临时列表中
        lo[row] = (sorted(set(row_list), key = row_list.index)) # 在不变顺序的前提下，去除List中的重复项
        
    return tuple(lo.values())


def parse_pdf(pdf_path):
    result_df = pd.DataFrame()
    pdf = pdfplumber.open(pdf_path)
    for page in pdf.pages:
        table = page.extract_table()
        df_detail = pd.DataFrame(table)
        result_df = pd.concat([df_detail, result_df], ignore_index=True)

    return result_df.values.tolist()



def generate_excel_data(email_name, val):
    excel_data = copy.copy(tool.email_format)
    try:     
        index = ''
        ecn_code = ''
        recv_date = ''
        product_type = ''
        change_info_publish = ''
        change_info = ''
        rework = ''
        change_type = ''
        ecn_reason = ''
        design = ''
        work_date = ''
        close_open = ''
        is_shared = ''
        other = ''
        mail_info = ''
        email_type = ''
        
        
        index = ''
        ecn_code = val['code']
        recv_date = val['recv_date']
        
        product_string = '' # 产品类型 对应了D列 来源: 邮件内容O列
        # 匹配产品类型
        if preview.preview_data(email_name.upper(), preview.type_servo):
            product_type = '伺服'
            product_string = "伺服"      
        elif preview.preview_data(email_name.upper(), preview.type_step):
            product_type = '步进'
            product_string = "步进"  
        elif preview.preview_data(email_name.upper(), preview.type_freq):
            product_type = '变频'
            product_string = "变频"  
        else:
            product_type = ''# 默认是空
        change_info_publish = val['reason']
        
        email_type = val['type']
        if val['type'] == 'product': # 区分邮件是ECN还是投产
            type_string = '' # 驱动器, 电机, 变频器, 步进驱动, 步进电机, 定制机 对应了F列的内容, 来源: 邮件内容
            ECN_string = "通用" # 对应了I列的ECN原因, 来源: 邮件内容或者变更内容发布(附件word或者PDF)
            if preview.preview_data(email_name, ['定制']) or preview.preview_data(val['reason'], ['定制']):
                ECN_string = "定制"
                type_string = "定制机"
            else:                
                if preview.preview_data(email_name.upper(), ['DS']):
                    type_string = '驱动器'
                elif preview.preview_data(email_name.upper(), ['MS']):
                    type_string = '电机'
                elif preview.preview_data(email_name.upper(), ['VH']):
                    type_string = '变频器'
                elif preview.preview_data(email_name.upper(), ['DP']):
                    type_string = '步进驱动'
                elif preview.preview_data(email_name.upper(), ['MP']):
                    type_string = '步进电机'

            
            change_info = (f'新投产{type_string}： {val["product_code"]}')
            rework = '否'
            change_type = '立即切换'
            ecn_reason = f"新品投产({ECN_string})"
            is_shared = '是'
            other = '配置表，图纸更新'
        else:
            if email_name.find(":") >= 0:
                change_info = email_name[email_name.index(":")+1:]
            elif email_name.find("：") >= 0:
                change_info = email_name[email_name.index("：")+1:]
            else:
                change_info = ''
            ecn_reason = ''
            is_shared = '否'
            other = '/'
        
            """
            if preview.preview_data(val['reason'], preview.need_rework) and not preview.preview_data(val['reason'], preview.need_rework_exclude):
                rework = append('返工')
                change_type = ('立即切换')
            else:
            """
            rework = ''
            change_type = ''
        
        
                    
        design = val['starter']
        work_date = ''
        close_open = ''
        mail_info = email_name
        
        excel_data['邮件类型'].append(email_type)
        excel_data['序号'].append(index)
        excel_data['ECN编号（研发）'].append(ecn_code)
        excel_data['接收时间'].append(recv_date)
        excel_data['产品类型'].append(product_type)
        excel_data['变更内容（发布）'].append(change_info_publish)
        excel_data['变更内容'].append(change_info)
        excel_data['半成品、成品是否返工'].append(rework)
        excel_data['切换类型（自然切换、立即切换）'].append(change_type)
        excel_data['ECN原因（缺料替代、换料投产、降本、质量问题、产品性能、产品外观提升、提升效率、设计问题、BOM修改、版本升级、新品投产等）'].append(ecn_reason)
        excel_data['方案设计'].append(design)
        excel_data['具体实施日期（自然切换：不跟进立即切换：写日期）'].append(work_date)
        excel_data['闭环情况(CLOSE/OPEN/NA)'].append(close_open)
        excel_data['相关内容是否已放共享(是/否)'].append(is_shared)
        excel_data['备注'].append(other)
        excel_data['邮件内容'].append(mail_info)
    except Exception as E:
        tool.log_show(email_name, "解析文件失败", str(E))
        excel_data['邮件类型'].append('')
        excel_data['序号'].append('')
        excel_data['ECN编号（研发）'].append('')
        excel_data['接收时间'].append('')
        excel_data['产品类型'].append('')
        excel_data['变更内容（发布）'].append('')
        excel_data['变更内容'].append('')
        excel_data['半成品、成品是否返工'].append('')
        excel_data['切换类型（自然切换、立即切换）'].append('')
        excel_data['ECN原因（缺料替代、换料投产、降本、质量问题、产品性能、产品外观提升、提升效率、设计问题、BOM修改、版本升级、新品投产等）'].append('')
        excel_data['方案设计'].append('')
        excel_data['具体实施日期（自然切换：不跟进立即切换：写日期）'].append('')
        excel_data['闭环情况(CLOSE/OPEN/NA)'].append('')
        excel_data['相关内容是否已放共享(是/否)'].append('')
        excel_data['备注'].append('')
        excel_data['邮件内容'].append(email_name)

    return excel_data


def copy_picture(work_path, excel_data):
    current_path = os.getcwd()
    os.chdir(work_path)

    ecn_root_path = os.getcwd()
    find_dir_ok = False
    find_dir_file_ok = False
    
    os_path = os.walk("./")
    for root, _, eml_files in os_path:
        os.chdir(ecn_root_path)
        if root.find("图") < 0:
            continue
        
        
        os.chdir(root)
        
        find_dir_ok = True
        for file in eml_files:
            if file.upper().endswith('DWG') or file.upper().endswith('PDF'): # 找到所有的CAD文件
                print(' '.join([tool.FAST_COPY_PATH, '/cmd=sync', '/auto_close', file, f'/to={tool.OUTPUT_CAD_PATH}']))
                subprocess.run([tool.FAST_COPY_PATH, '/cmd=sync', '/auto_close', file, f'/to={tool.OUTPUT_CAD_PATH}'])
                find_dir_file_ok = True
            elif file.upper().endswith('IGS') or file.upper().endswith('STEP'): # 找到所有的3D文件
                print(' '.join([tool.FAST_COPY_PATH, '/cmd=sync', '/auto_close', file, f'/to={tool.OUTPUT_3D_PATH}']))
                subprocess.run([tool.FAST_COPY_PATH, '/cmd=sync', '/auto_close', file, f'/to={tool.OUTPUT_3D_PATH}'])
                find_dir_file_ok = True

        os.chdir(ecn_root_path)
        
    if not find_dir_ok:
        tool.log_show('WARNING', f"{excel_data['邮件内容']} 没有找到对应的 外形图及二维图 文件夹")
        if excel_data['邮件类型'] == 'product':
            excel_data['备注'] = '配置表更新，无图纸'

    elif not find_dir_file_ok:
        tool.log_show('WARNING', f"{excel_data['邮件内容']} 没有找到对应的 CAD文件或者3D文件")
        if excel_data['邮件类型'] == 'product':
            excel_data['备注'] = '配置表更新，无图纸'
    os.chdir(current_path)

    return True


class ZipObject:
    def __init__(self, email_info, email_name, zip_name) -> None:
        self.email_info = email_info
        self.email_name = email_name
        self.zip_name = zip_name
        self.zip_path = tool.get_zip_path(self.zip_name)
        self.file_list = []
        self.parse_enable = True


    def clear(self):
        self.file_list.clear()
        self.parse_enable = True


    def scan(self):
        if not os.path.exists(self.zip_path):
            tool.log_show("ERROR", "Zip路径无效: ", self.zip_path)
            return False
        else:
            tool.x_zip(self.zip_path, os.path.join(tool.DIR_PATH, self.zip_name))

            maybe_target = []

            os_walk = os.walk(os.path.join(tool.DIR_PATH, self.zip_name))
            for root, _, files in os_walk:
                for file in files:
                    # 过滤掉无效的文件类型
                    if file.find('~$') >= 0 or file.find("签字版") >= 0:
                        continue

                    if os.path.splitext(file)[-1] not in ('.doc', '.docx', '.pdf'):
                        continue

                    if is_ecn(file):
                        self.file_list.append(os.path.join(root, file))
                        return True

                    if is_product(file):
                        self.file_list.append(os.path.join(root, file))
                        return True

                    # 有且仅有一个ecn/投产
                    maybe_target.append(os.path.join(root, file))
            
            if len(maybe_target) == 1:
                self.file_list.append(maybe_target[0])
                return True
            
            return False


    def get_list(self):
        return self.file_list


    def parse(self, parse_tree:dict):
        parse_data_list = []

        for file_path_dict in parse_tree:
            for file_path in file_path_dict.keys():
                parse_data = self.parse_single(file_path)
                if parse_data is None:
                    continue


                excel_data = generate_excel_data(self.email_name, dict(**parse_data, **self.email_info))
                copy_picture(os.path.join(tool.DIR_PATH, self.zip_name), excel_data)
                del excel_data['邮件类型']
                excel_data = tuple(map(lambda x: x[0], excel_data.values()))
                parse_data_list.append(excel_data)
        
        return parse_data_list


    def parse_single(self, file_path: str) -> dict:
        parse_data = None

        if file_path.endswith('.doc') or file_path.endswith('.docx'):
            parse_data = parse_word(file_path)
        elif file_path.endswith('.pdf'):
            parse_data = parse_pdf(file_path)

        if parse_data is None or len(parse_data) == 0:
            return None

        if is_product(file_path):
            return parse_production(parse_data)
        elif is_ecn(file_path):
            return parse_ecn(parse_data)
        else:
            return None
'''
Descripttion: 
Author: QYJ
version: 
Date: 2024-04-20 10:33:12
LastEditors: QYJ
LastEditTime: 2024-04-20 15:06:13
'''
'''
Descripttion: 
Author: QYJ
version: 
Date: 2024-04-20 10:33:12
LastEditors: QYJ
LastEditTime: 2024-04-20 14:43:02
'''
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
from tool import *


CHECK_STATE = (Qt.CheckState.Unchecked, Qt.CheckState.Checked)


class CustomTreeWidgetItem(QTreeWidgetItem):
    def __init__(self, *args, **kwargs):
        self.type = None
        super().__init__(*args, **kwargs)
        

class EmailTreeWidget(QTreeWidget):

    def __init__(self, parent: QWidget | None = ...) -> None:
        super().__init__(parent)
        self.parent = parent.parent()
        self.setColumnCount(1)
        self.setHeaderLabels(['Emails'])
        self.clear_tree()

    def clear_tree(self):
        self.clear()

    def email_select_slot(self, *arg):
        index = self.parent.email_select_button.text() == "邮件全选"
        text = ("邮件全选", "邮件取消全选")[index]
        self.parent.email_select_button.setText(text)

        email_cnt = self.topLevelItemCount()
        for email_index in range(email_cnt):
            self.topLevelItem(email_index).setCheckState(0, CHECK_STATE[index])

    def zip_select_slot(self, *arg):
        index = self.parent.zip_select_button.text() == "压缩包全选"
        text = ("压缩包全选", "压缩包取消全选")[index]
        self.parent.zip_select_button.setText(text)

        email_cnt = self.topLevelItemCount()
        for email_index in range(email_cnt):
            zip_cnt = self.topLevelItem(email_index).childCount()

            for zip_index in range(zip_cnt):
                self.topLevelItem(email_index).child(zip_index).setCheckState(0, CHECK_STATE[index])

    def file_select_slot(self, *arg):
        index = self.parent.file_select_button.text() == "文件全选"
        text = ("文件全选", "文件取消全选")[index]
        self.parent.file_select_button.setText(text)

        email_cnt = self.topLevelItemCount()
        for email_index in range(email_cnt):
            zip_cnt = self.topLevelItem(email_index).childCount()

            for zip_index in range(zip_cnt):

                file_cnt = self.topLevelItem(email_index).child(zip_index).childCount()
                for file_index in range(file_cnt):
                    self.topLevelItem(email_index).child(zip_index).child(file_index).setCheckState(0, CHECK_STATE[index])

    def create_tree(self, eml_list: dict):
        items = []
        for eml_name, zip_list in eml_list.items():
            item_eml = CustomTreeWidgetItem([eml_name])
            item_eml.type = 'eml'
            item_eml.setCheckState(0, Qt.CheckState.Checked)
            item_eml.setToolTip(0, eml_name)

            for zip_name, files_name in zip_list.items():
                item_zip = CustomTreeWidgetItem([zip_name])
                item_zip.type = 'zip'
                item_zip.setCheckState(0, Qt.CheckState.Checked)
                item_zip.setToolTip(0, zip_name)

                for file_name in files_name:
                    item_file = CustomTreeWidgetItem([os.path.split(file_name)[-1]])
                    item_file.type = 'file'
                    item_file.setCheckState(0, Qt.CheckState.Checked)
                    item_file.setToolTip(0, file_name)
                    item_zip.addChild(item_file)
                
                item_eml.addChild(item_zip)
            
            items.append(item_eml)
        self.insertTopLevelItems(0, items)
        
    def contextMenuEvent(self, mev: QContextMenuEvent) -> None:
        root_item = self.selectedItems()[0]
        if root_item.type == 'zip':
            select = get_choose_menu([["添加", ]], mev)
            
            if select == '添加':
                filepath = get_single_file("选择你要添加的文件", ('*.doc', "*.docx", "*.pdf"))
                if not os.path.exists(filepath):
                    return
                
                new_item = CustomTreeWidgetItem([os.path.split(filepath)[-1]])
                new_item.setCheckState(0, Qt.CheckState.Checked)
                new_item.setToolTip(0, filepath)
                root_item.addChild(new_item)
            
        elif root_item.type == 'file':
            select = get_choose_menu([["删除", ]], mev)
            if select == '删除':
                root_item.parent().removeChild(root_item)
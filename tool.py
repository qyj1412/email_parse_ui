import os
import subprocess
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *
import shutil
import time


ROOT_PATH = os.path.split(os.path.abspath(__file__))[0]
EMAIL_PATH = os.path.join(ROOT_PATH, 'emails')
ZIP_PATH = os.path.join(ROOT_PATH, 'zips')
DIR_PATH = os.path.join(ROOT_PATH, 'dir')
TOOL_PATH = os.path.join(ROOT_PATH, 'tools')
TOOL_7Z_PATH = os.path.join(TOOL_PATH, '7z/7z.exe')
OUTPUT_PATH = os.path.join(ROOT_PATH, 'output')
OUTPUT_CSV_PATH = os.path.join(OUTPUT_PATH, 'output.csv')
OUTPUT_3D_PATH = os.path.join(OUTPUT_PATH, '3D')
OUTPUT_CAD_PATH = os.path.join(OUTPUT_PATH, 'CAD')
FAST_COPY_PATH = os.path.join(TOOL_PATH, "FastCopy/FastCopy.exe")
LOG_ELEMENT: QPlainTextEdit = None
ROOT_WINDOW: QMainWindow = None
email_format = {'序号': [], 'ECN编号（研发）': [], '接收时间': [], '产品类型': [],
    '变更内容（发布）': [], '变更内容': [], '半成品、成品是否返工': [] , 
    '切换类型（自然切换、立即切换）': [], 
    'ECN原因（缺料替代、换料投产、降本、质量问题、产品性能、产品外观提升、提升效率、设计问题、BOM修改、版本升级、新品投产等）': [], 
    '方案设计': [], '具体实施日期（自然切换：不跟进立即切换：写日期）': [], '闭环情况(CLOSE/OPEN/NA)': [], 
    '相关内容是否已放共享(是/否)': [], 
    '备注': [], '邮件内容': [], '邮件类型': []
}


CHECK_STATE_TO_BOOL = {
    Qt.CheckState.Checked: True,
    Qt.CheckState.Unchecked: False,
}

if not os.path.exists(EMAIL_PATH):
    os.mkdir(EMAIL_PATH)

if not os.path.exists(ZIP_PATH):
    os.mkdir(ZIP_PATH)

if not os.path.exists(DIR_PATH):
    os.mkdir(DIR_PATH)

if not os.path.exists(OUTPUT_PATH):
    os.mkdir(OUTPUT_PATH)

if not os.path.exists(OUTPUT_3D_PATH):
    os.mkdir(OUTPUT_3D_PATH)

if not os.path.exists(OUTPUT_CAD_PATH):
    os.mkdir(OUTPUT_CAD_PATH)


def clear_output_path():
    if os.path.exists(OUTPUT_PATH):
        shutil.rmtree(OUTPUT_PATH)
    os.mkdir(OUTPUT_PATH)

    os.mkdir(OUTPUT_3D_PATH)

    os.mkdir(OUTPUT_CAD_PATH)


def get_emails():
    if not os.path.exists(EMAIL_PATH):
        return ()
    else:
        # return tuple(map(lambda x: os.path.splitext(x)[0], filter(lambda x: os.path.splitext(x)[-1] == '.eml', os.listdir(EMAIL_PATH))))
        return tuple(filter(lambda x: os.path.splitext(x)[-1] == '.eml', os.listdir(EMAIL_PATH)))


def get_zips():
    if not os.path.exists(ZIP_PATH):
        return ()
    else:
        return tuple(filter(lambda x: os.path.splitext(x)[-1] in ('.zip', '.7z', '.rar'), os.listdir(ZIP_PATH)))


def get_email_path(email_name):
    return os.path.join(EMAIL_PATH, email_name)


def get_zip_path(zip_name):
    return os.path.join(ZIP_PATH, zip_name)


def get_choose_menu(menu_lists: list[list], contentMenuEvent):
    menu_item = QMenu()

    for menu_list in menu_lists:
        for menu in menu_list:
            new_action = QAction(menu, menu_item)
            menu_item.addAction(new_action)
        menu_item.addSeparator()
    
    res = menu_item.exec_(contentMenuEvent.globalPos())
    if res is not None:
        return res.text()
    
    return None


def get_single_file(title_string, file_types=tuple[str]):
    filepath, _ = QFileDialog.getOpenFileName(
        None,             # 父窗口对象
        "选择你要上传的图片", # 标题
        ROOT_PATH,        # 起始目录
        f"类型 ({' '.join(file_types)})" # 选择类型过滤项，过滤内容在括号中
    )
    
    return filepath


def log_clear():
    global LOG_ELEMENT
    if LOG_ELEMENT is None:
        return
    LOG_ELEMENT.clear()


def log_show(level, *msg):
    global LOG_ELEMENT
    if LOG_ELEMENT is None:
        return
    
    LOG_ELEMENT.insertPlainText(f'{time.asctime()}: {level} : {"".join(msg)}\n\n')


def x_zip(zip_path, zip_output_path):
    subprocess.run([TOOL_7Z_PATH, 'x', zip_path, f'-o{zip_output_path}'], stdout=subprocess.DEVNULL)


def clear_zip_path():
    if os.path.exists(ZIP_PATH):
        os.system(f"rmdir /Q /S {ZIP_PATH}")
    os.mkdir(ZIP_PATH)


def clear_dir_path():
    if os.path.exists(DIR_PATH):
        os.system(f"rmdir /Q /S {DIR_PATH}")
    os.mkdir(DIR_PATH)


if __name__ == '__main__':
    print(get_emails())
from Ui_MainWidget import Ui_MainWindow
from PySide6.QtWidgets import QMainWindow
from PySide6.QtCore import *
from PySide6.QtGui import *
import tool
import os
import csv

import EmailManager

class MainWidget(Ui_MainWindow, QMainWindow):
    def __init__(self, app, parent=None) -> None:
        global LOG_ELEMENT
        super().__init__(parent)
        self.setupUi(self)
        self.eml_manager = EmailManager.EmailManager()

        tool.ROOT_WINDOW = self
        tool.LOG_ELEMENT = self.log_shower
        tool.LOG_ELEMENT.clear()


    def menu_triggle_scan(self):
        tool.clear_zip_path()
        tool.clear_dir_path()
        self.email_tree.clear_tree()
        if self.eml_manager.scan():
            self.email_tree.create_tree(self.eml_manager.get_list())


    def menu_triggle_parse(self):
        email_cnt = self.email_tree.topLevelItemCount()
        parse_tree = {}
        for email_index in range(email_cnt):
            email_root = self.email_tree.topLevelItem(email_index)
            if not tool.CHECK_STATE_TO_BOOL[email_root.checkState(0)]:
                continue
            parse_tree[email_root.text(0)] = {}
            parse_tree[email_root.text(0)]['childs'] = []
            zip_cnt = email_root.childCount()


            for zip_index in range(zip_cnt):
                zip_root = email_root.child(zip_index)
                if not tool.CHECK_STATE_TO_BOOL[zip_root.checkState(0)]:
                    continue
                zip_tree = {}
                zip_tree[zip_root.text(0)] = {}
                zip_tree[zip_root.text(0)]['childs'] = []

                file_cnt = zip_root.childCount()
                for file_index in range(file_cnt):
                    file_root = zip_root.child(file_index)
                    if not tool.CHECK_STATE_TO_BOOL[file_root.checkState(0)]:
                        continue
                    file_tree = {}
                    file_tree[file_root.toolTip(0)] = {}
                    file_tree[file_root.toolTip(0)]['childs'] = []

                    zip_tree[zip_root.text(0)]['childs'].append(file_tree)
                
                parse_tree[email_root.text(0)]['childs'].append(zip_tree)

        tool.clear_output_path()
        res = self.eml_manager.parse(parse_tree)
        
        # 将解析结果写入csv
        with open(tool.OUTPUT_CSV_PATH, 'w', encoding='utf-8') as fp:
            writer = csv.writer(fp)
            writer.writerow(tuple(tool.email_format.keys()))
            writer.writerows(res)

        # 拷贝3D CAD文件
        


    def menu_triggle(self, menu: QAction):
        if menu.text() == 'Scan':
            self.menu_triggle_scan()
        elif menu.text() == 'Parse':
            self.menu_triggle_parse()
        elif menu.text() == '打开邮件列表':
            os.startfile(tool.EMAIL_PATH)
        elif menu.text() == '打开压缩包界面':
            os.startfile(tool.DIR_PATH)
        elif menu.text() == '打开解压文件界面':
            os.startfile(tool.ZIP_PATH)
        elif menu.text() == '打开输出文件界面':
            os.startfile(tool.OUTPUT_PATH)
            
    def email_tree_doubleclick(self, model_index: QModelIndex):
        pass
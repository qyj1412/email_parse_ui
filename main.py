from PySide6 import QtWidgets
from MainWidget import MainWidget
import qt_material

if __name__ =='__main__':
    app = QtWidgets.QApplication([])
    window = MainWidget(app)
    qt_material.apply_stylesheet(app, theme='light_purple_500.xml', invert_secondary=False)
    window.show()
    app.exec()